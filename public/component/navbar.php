<!-- <div id="nav">
    <nav class="font-Montserrat bg-white sm:px-4 px-4 py-1  fixed w-full z-20 top-0 left-0">
        <div class=" flex flex-wrap items-center justify-between mx-auto">
            <a href="#" class="flex items-center">
                <img src="assets/images/navbar/logo.svg" class="w-[106px] h-[28px] mr-3 md:hidden" alt="img">
            </a>
            <div class="flex md:order-2">

                <button data-collapse-toggle="navbar-sticky" type="button"
                    class="inline-flex items-center p-2 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
                    aria-controls="navbar-sticky" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>
            </div>
            <div class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="navbar-sticky">
                <ul
                    class="flex flex-col lg:items-center p-4 mt-4  md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0 ">
                    <li>
                        <a href="#"> <img src="assets/images/navbar/logo.svg"
                                class="w-[106px] h-[28px] mr-3 max-lg:hidden" alt="img">
                        </a>
                    </li>
                    <li>
                        <a href="#" class="block py-2 pl-3 pr-5 text-[#494652] font-bold" aria-current="page">Home</a>
                    </li>
                    <li>
                        <a href="index.php#tentang" class="block py-2 pl-3 pr-5 text-[#494652] font-bold">Tentang</a>
                    </li>
                    <li>
                        <a href="index.php#talent" class="block py-2 pl-3 pr-5 text-[#494652] font-bold">KOL</a>
                    </li>
                    <li>
                        <a href="index.php#event" class="block py-2 pl-3 pr-5 text-[#494652] font-bold">Event</a>
                    </li>
                    <li>
                        <a href="index.php#footer" class="block py-2 pl-3 pr-5 text-[#494652] font-bold">Kontak</a>
                    </li>
                    <li>
                        <a href="#"> <img src="assets/images/navbar/search.svg"
                                class="w-[106px] h-[28px] mr-3 " alt="img">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div> -->


<!-- <nav class="font-Montserrat bg-white border-gray-200 px-2 sm:px-4 py-2.5 rounded fixed w-full z-20 top-0 left-0" style="box-shadow: 0px 4px 25px rgba(0, 0, 0, 0.35);">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto">

        <div class=" flex flex-wrap items-center justify-between mx-auto">
            <a href="https://flowbite.com/" class="flex items-center">
                <img src="assets/images/navbar/logo.svg" class="mr-3 w-[106px] h-[28px]" alt="Flowbite Logo" />
            </a>
            <div class="flex md:order-2">
                <button type="button" data-collapse-toggle="navbar-search" aria-controls="navbar-search"
                    aria-expanded="false"
                    class="md:hidden text-gray-500  hover:bg-gray-100  focus:outline-none focus:ring-4 focus:ring-gray-200 rounded-lg text-sm p-2.5 mr-1">
                    <svg class="w-5 h-5" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                            clip-rule="evenodd"></path>
                    </svg>
                    <span class="sr-only">Search</span>
                </button>
                <div class="relative hidden md:block">
                    <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                        <svg class="w-5 h-5 text-gray-500" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                                clip-rule="evenodd"></path>
                        </svg>
                        <span class="sr-only">Search icon</span>
                    </div>
                    <input type="text" id="search-navbar"
                        class="block w-full p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg  focus:ring-blue-500 focus:border-blue-500"
                        placeholder="Search...">
                </div>
                <button data-collapse-toggle="navbar-search" type="button"
                    class="inline-flex items-center p-2 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 "
                    aria-controls="navbar-search" aria-expanded="false">
                    <span class="sr-only">Open menu</span>
                    <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>
            </div>
            <div class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="navbar-search">
                <div class="relative mt-3 md:hidden">
                    <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                        <svg class="w-5 h-5 text-gray-500" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                            xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                                clip-rule="evenodd"></path>
                        </svg>
                    </div>
                    <input type="text" id="search-navbar"
                        class="block w-full p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg  focus:ring-blue-500 focus:border-blue-500"
                        placeholder="Search...">
                </div>
                <ul
                    class="font-semibold mt-3 flex flex-col border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 xl:space-x-12 md:mt-0 md:text-sm md:font-bold md:border-0 md:bg-white">
                    <li>
                        <a href="#"
                            class="block py-2 pl-3 pr-4 text-[#494652] rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0"
                            aria-current="page">Home</a>
                    </li>
                    <li>
                        <a href="index.php#tentang"
                            class="block py-2 pl-3 pr-4 text-[#494652] rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 ">Tentang</a>
                    </li>
                    <li>
                        <a href="index.php#talent"
                            class="block py-2 pl-3 pr-4 text-[#494652] rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">KOL</a>
                    </li>
                    <li>
                        <a href="index.php#event"
                            class="block py-2 pl-3 pr-4 text-[#494652] rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">Event</a>
                    </li>
                    <li>
                        <a href="index.php#footer"
                            class="block py-2 pl-3 pr-4 text-[#494652] rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">Kontak</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav> -->

<nav class="font-Montserrat bg-white border-gray-200 px-4 sm:px-4 py-2.5 rounded fixed w-full z-20 top-0 left-0"
    style="box-shadow: 0px 4px 25px rgba(0, 0, 0, 0.35);">
    <div class="lg:max-w-[970px] lg:w-full lg:m-auto">

        <div class="container flex flex-wrap items-center justify-between mx-auto">
            <a href="index.php" class="flex items-center">
                <img src="assets/images/navbar/logo.svg" class="mr-3 w-[106px] h-[-28px]" alt="img">
            </a>
            <div class="flex md:order-2">

                <!-- <button type="button"
                    class="text-gray-500  hover:bg-gray-100 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-0 md:mr-0">
                    <svg class="w-5 h-5" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button> -->

                <button id="dropdownNavbarLink" data-dropdown-toggle="dropdownNavbar" aria-label="button"
                    class="flex items-center justify-between w-full py-2 pl-3 pr-4 font-medium text-gray-700 rounded hover:bg-gray-100 md:border-0 md:p-0 md:w-auto">
                    <svg class="w-5 h-5" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>

                <div id="dropdownNavbar"
                    class="z-10 hidden font-normal bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700 dark:divide-gray-600">
                    <ul class="" aria-labelledby="dropdownLargeButton">
                        <li>
                            <input type="text" id="search-navbar"
                                class="block w-full p-2 text-sm text-gray-900 border border-gray-300 rounded-lg  focus:ring-blue-500 focus:border-blue-500"
                                placeholder="Search...">
                        </li>
                    </ul>
                </div>

                <button data-collapse-toggle="navbar-sticky" type="button"
                    class="inline-flex items-center p-2 text-sm text-gray-700 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 "
                    aria-controls="navbar-sticky" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                            clip-rule="evenodd"></path>
                    </svg>
                </button>
            </div>
            <div class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="navbar-sticky">
                <ul
                    class="font-semibold mt-3 flex flex-col border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 xl:space-x-28 md:mt-0 md:text-sm md:font-bold md:border-0 md:bg-white">
                    <li>
                        <a href="#header"
                            class="block py-2 pl-3 pr-4 text-[#494652] rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0"
                            aria-current="page">Home</a>
                    </li>
                    <li>
                        <a href="#tentang"
                            class="block py-2 pl-3 pr-4 text-[#494652] rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 ">Tentang</a>
                    </li>
                    <li>
                        <a href="#talent"
                            class="block py-2 pl-3 pr-4 text-[#494652] rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">KOL</a>
                    </li>
                    <li>
                        <a href="#event"
                            class="block py-2 pl-3 pr-4 text-[#494652] rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">Event</a>
                    </li>
                    <li>
                        <a href="#footer"
                            class="block py-2 pl-3 pr-4 text-[#494652] rounded hover:bg-gray-300 md:hover:bg-transparent md:hover:text-blue-700 md:p-0">Kontak</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>