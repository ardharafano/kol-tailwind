<div id="footer">
    <div class="font-Montserrat">
        <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
            <h1 class="font-bold text-xl text-center py-5">Ikuti Kami</h1>
            <div class="flex justify-center items-center">
                <a href="#">
                    <img src="assets/images/footer/fb.svg"
                        class="my-0 mx-3 border-[1px] border-[#dadada] rounded-xl w-12 h-12 p-2" alt="img">
                </a>

                <a href="#">
                    <img src="assets/images/footer/twt.svg"
                        class="my-0 mx-3 border-[1px] border-[#dadada] rounded-xl w-12 h-12 p-2"" alt=" img">
                </a>

                <a href="#">
                    <img src="assets/images/footer/yt.svg"
                        class="my-0 mx-3 border-[1px] border-[#dadada] rounded-xl w-12 h-12 p-2"" alt=" img">
                </a>

                <a href="#">
                    <img src="assets/images/footer/ig.svg"
                        class="my-0 mx-3 border-[1px] border-[#dadada] rounded-xl w-12 h-12 p-2"" alt=" img">
                </a>
            </div>
            <p class="text-center mt-3">Dapatkan informasi terkini dan terbaru yang dikirimkan langsung ke Inbox
                anda
            </p>
        </div>
    </div>

    <div class="font-Montserrat bg-[#43239E] py-5 my-5">
        <div class="lg:max-w-[970px] lg:w-full lg:m-auto">
            <div class="foot flex flex-wrap gap-3 justify-center items-center">
                <a href="#" class="text-white border-r-[1px] border-[#dadada] pr-4">
                    Redaksi
                </a>
                <a href="#" class="text-white border-r-[1px] border-[#dadada] pr-4">
                    Kontak
                </a>
                <a href="#" class="text-white border-r-[1px] border-[#dadada] pr-4">
                    Tentang Kami
                </a>
                <a href="#" class="text-white border-r-[1px] border-[#dadada] pr-4">
                    Karir
                </a>
                <a href="#" class="text-white border-r-[1px] border-[#dadada] pr-4">
                    Pedoman Media Siber
                </a>
                <a href="#" class="text-white pr-4">
                    Site Map
                </a>
            </div>
        </div>
    </div>

    <div class="font-Montserrat mb-3">
        <div class="lg:max-w-[970px] lg:w-full lg:m-auto">
            <p class="text-center">© 2023 suara.com - All Rights Reserved.</p>
        </div>
    </div>
</div>