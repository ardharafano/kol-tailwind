<!-- <div id="talent" class="font-Montserrat py-12 px-4">
    <div class="lg:w-[970px] lg:m-auto">
        <div class="container">
            <div class="text-center">
                <h4 class="text-[#A8A8A8] font-bold text-2xl">KOL</h4>
                <h2 class="text-[#43239E] font-bold text-3xl mb-3">KOL Talent</h3>
            </div>
            <div class="flex flex-wrap justify-center">
                <div class="mb-4">
                    <ul class="flex flex-wrap justify-center -mb-px text-sm font-medium text-center" id="myTab"
                        data-tabs-toggle="#myTabContent" role="tablist">
                        <li class="mr-2" role="presentation">
                            <button
                                class="inline-block p-4 border-b-4 rounded-t-lg text-blue-900 hover:text-blue-900 dark:text-blue-900 dark:hover:text-blue-900"
                                id="makanan-tab" data-tabs-target="#makanan" type="button" role="tab"
                                aria-controls="makanan" aria-selected="true">Makanan</button>
                        </li>
                        <li class="mr-2" role="presentation">
                            <button
                                class="inline-block p-4 border-b-4 rounded-t-lg text-blue-900 hover:text-blue-900 dark:text-blue-900 dark:hover:text-blue-900"
                                id="teknologi-tab" data-tabs-target="#teknologi" type="button" role="tab"
                                aria-controls="teknologi" aria-selected="false">Teknologi</button>
                        </li>
                        <li class="mr-2" role="presentation">
                            <button
                                class="inline-block p-4 border-b-4 rounded-t-lg text-blue-900 hover:text-blue-900 dark:text-blue-900 dark:hover:text-blue-900"
                                id="hiburan-tab" data-tabs-target="#hiburan" type="button" role="tab"
                                aria-controls="hiburan" aria-selected="false">Hiburan</button>
                        </li>
                        <li class="mr-2" role="presentation">
                            <button
                                class="inline-block p-4 border-b-4 rounded-t-lg text-blue-900 hover:text-blue-900 dark:text-blue-900 dark:hover:text-blue-900"
                                id="travel-tab" data-tabs-target="#travel" type="button" role="tab"
                                aria-controls="travel" aria-selected="false">Travel</button>
                        </li>
                        <li class="mr-2" role="presentation">
                            <button
                                class="inline-block p-4 border-b-4 rounded-t-lg text-blue-900 hover:text-blue-900 dark:text-blue-900 dark:hover:text-blue-900"
                                id="sport-tab" data-tabs-target="#sport" type="button" role="tab" aria-controls="sport"
                                aria-selected="false">Sport</button>
                        </li>
                        <li class="mr-2" role="presentation">
                            <button
                                class="inline-block p-4 border-b-4 rounded-t-lg text-blue-900 hover:text-blue-900 dark:text-blue-900 dark:hover:text-blue-900"
                                id="gaming-tab" data-tabs-target="#gaming" type="button" role="tab"
                                aria-controls="gaming" aria-selected="false">Gaming</button>
                        </li>
                        <li class="mr-2" role="presentation">
                            <button
                                class="inline-block p-4 border-b-4 rounded-t-lg text-blue-900 hover:text-blue-900 dark:text-blue-900 dark:hover:text-blue-900"
                                id="fashion-tab" data-tabs-target="#fashion" type="button" role="tab"
                                aria-controls="fashion" aria-selected="false">Fashion</button>
                        </li>
                        <li class="mr-2" role="presentation">
                            <button
                                class="inline-block p-4 border-b-4 rounded-t-lg text-blue-900 hover:text-blue-900 dark:text-blue-900 dark:hover:text-blue-900"
                                id="youtuber-tab" data-tabs-target="#youtuber" type="button" role="tab"
                                aria-controls="youtuber" aria-selected="false">Youtuber</button>
                        </li>
                        <li class="mr-2" role="presentation">
                            <button
                                class="inline-block p-4 border-b-4 rounded-t-lg text-blue-900 hover:text-blue-900 dark:text-blue-900 dark:hover:text-blue-900"
                                id="tiktok-tab" data-tabs-target="#tiktok" type="button" role="tab"
                                aria-controls="tiktok" aria-selected="false">Tiktok</button>
                        </li>
                    </ul>
                </div>
                <div id="myTabContent">
                    <div class="hidden p-4 rounded-lg " id="makanan" role="tabpanel" aria-labelledby="profile-tab">

                        <section class="splide makanan" aria-label="Splide Basic HTML Example">
                            <div class="splide__arrows">
                                <button class="bg-blue-900 splide__arrow splide__arrow--prev">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                                <button class="bg-blue-900 splide__arrow splide__arrow--next">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                            </div>
                            <div class="splide__track">
                                <ul class="splide__list">
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                </ul>
                            </div>
                        </section>

                    </div>
                    <div class="hidden p-4 rounded-lg" id="teknologi" role="tabpanel" aria-labelledby="teknologi-tab">

                        <section class="splide teknologi" aria-label="Splide Basic HTML Example">
                            <div class="splide__arrows">
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--prev">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--next">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                            </div>
                            <div class="splide__track">
                                <ul class="splide__list">
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                </ul>
                            </div>
                        </section>

                    </div>
                    <div class="hidden p-4 rounded-lg" id="hiburan" role="tabpanel" aria-labelledby="hiburan-tab">

                        <section class="splide hiburan" aria-label="Splide Basic HTML Example">
                            <div class="splide__arrows">
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--prev">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--next">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                            </div>
                            <div class="splide__track">
                                <ul class="splide__list">
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                </ul>
                            </div>
                        </section>

                    </div>
                    <div class="hidden p-4 rounded-lg" id="travel" role="tabpanel" aria-labelledby="travel-tab">

                        <section class="splide travel" aria-label="Splide Basic HTML Example">
                            <div class="splide__arrows">
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--prev">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--next">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                            </div>
                            <div class="splide__track">
                                <ul class="splide__list">
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                </ul>
                            </div>
                        </section>

                    </div>
                    <div class="hidden p-4 rounded-lg" id="sport" role="tabpanel" aria-labelledby="sport-tab">

                        <section class="splide sport" aria-label="Splide Basic HTML Example">
                            <div class="splide__arrows">
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--prev">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--next">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                            </div>
                            <div class="splide__track">
                                <ul class="splide__list">
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                </ul>
                            </div>
                        </section>

                    </div>
                    <div class="hidden p-4 rounded-lg" id="gaming" role="tabpanel" aria-labelledby="gaming-tab">

                        <section class="splide gaming" aria-label="Splide Basic HTML Example">
                            <div class="splide__arrows">
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--prev">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--next">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                            </div>
                            <div class="splide__track">
                                <ul class="splide__list">
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                </ul>
                            </div>
                        </section>

                    </div>
                    <div class="hidden p-4 rounded-lg" id="fashion" role="tabpanel" aria-labelledby="fashion-tab">

                        <section class="splide fashion" aria-label="Splide Basic HTML Example">
                            <div class="splide__arrows">
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--prev">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--next">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                            </div>
                            <div class="splide__track">
                                <ul class="splide__list">
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                </ul>
                            </div>
                        </section>

                    </div>
                    <div class="hidden p-4 rounded-lg" id="youtuber" role="tabpanel" aria-labelledby="youtuber-tab">

                        <section class="splide youtuber" aria-label="Splide Basic HTML Example">
                            <div class="splide__arrows">
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--prev">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--next">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                            </div>
                            <div class="splide__track">
                                <ul class="splide__list">
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                </ul>
                            </div>
                        </section>

                    </div>
                    <div class="hidden p-4 rounded-lg" id="tiktok" role="tabpanel" aria-labelledby="tiktok-tab">

                        <section class="splide tiktok" aria-label="Splide Basic HTML Example">
                            <div class="splide__arrows">
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--prev">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                                <button class="bg-gray-900 shadow splide__arrow splide__arrow--next">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke-width="1.5" stroke="currentColor" class="w-6 h-6 text-white">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
                                    </svg>
                                </button>
                            </div>
                            <div class="splide__track">
                                <ul class="splide__list">
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                    <li class="splide__slide p-4">
                                        <img src="assets/images/talent/talent.png" alt="img"
                                            class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                                    </li>
                                </ul>
                            </div>
                        </section>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->


<div id="talent" class="font-Montserrat py-16 px-4" id="fade-up-card" data-hs="fade up">
    <div class="text-center">
        <h4 class="text-[#A8A8A8] font-bold text-2xl">KOL</h4>
        <h2 class="text-[#43239E] font-bold text-3xl mb-3">KOL Talent</h3>
    </div>
    <ul class="nav nav-pills mb-3 justify-center" id="pills-tab" role="tablist">
        <li class="nav-item" role="presentation">
            <button class="nav-link active" id="pills-makanan-tab" data-bs-toggle="pill" data-bs-target="#pills-makanan"
                type="button" role="tab" aria-controls="pills-makanan" aria-selected="true">Makanan</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-teknologi-tab" data-bs-toggle="pill" data-bs-target="#pills-teknologi"
                type="button" role="tab" aria-controls="pills-teknologi" aria-selected="false">Teknologi</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-hiburan-tab" data-bs-toggle="pill" data-bs-target="#pills-hiburan"
                type="button" role="tab" aria-controls="pills-hiburan" aria-selected="false">Hiburan</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-travel-tab" data-bs-toggle="pill" data-bs-target="#pills-travel"
                type="button" role="tab" aria-controls="pills-travel" aria-selected="false">Travel</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-sport-tab" data-bs-toggle="pill" data-bs-target="#pills-sport"
                type="button" role="tab" aria-controls="pills-sport" aria-selected="false">Sport</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-gaming-tab" data-bs-toggle="pill" data-bs-target="#pills-gaming"
                type="button" role="tab" aria-controls="pills-gaming" aria-selected="false">Gaming</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-fashion-tab" data-bs-toggle="pill" data-bs-target="#pills-fashion"
                type="button" role="tab" aria-controls="pills-fashion" aria-selected="false">Fashion</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-youtuber-tab" data-bs-toggle="pill" data-bs-target="#pills-youtuber"
                type="button" role="tab" aria-controls="pills-youtuber" aria-selected="false">Youtuber</button>
        </li>
        <li class="nav-item" role="presentation">
            <button class="nav-link" id="pills-tiktok-tab" data-bs-toggle="pill" data-bs-target="#pills-tiktok"
                type="button" role="tab" aria-controls="pills-tiktok" aria-selected="false">Tiktok</button>
        </li>
    </ul>

    <div class="lg:max-w-[970px] lg:w-full lg:m-auto" id="fade-up-card" data-hs="fade up">
        <div class="tab-content" id="pills-tabContent">

            <div class="tab-pane fade show active" id="pills-makanan" role="tabpanel"
                aria-labelledby="pills-makanan-tab" tabindex="0">

                <section class="splide makanan" aria-label="Splide Basic HTML Example">
                    <ul class="splide__pagination"></ul>
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <button class="splide__pagination__page" type="button" role="tab"
                                aria-controls="splide01-slide02" aria-label="Go to slide 2" tabindex="-1">
                            </button>
                        </ul>
                    </div>
                </section>

            </div>

            <div class="tab-pane fade" id="pills-teknologi" role="tabpanel" aria-labelledby="pills-teknologi-tab"
                tabindex="0">

                <section class="splide teknologi" aria-label="Splide Basic HTML Example">
                    <ul class="splide__pagination"></ul>
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                        </ul>
                    </div>
                </section>

            </div>

            <div class="tab-pane fade" id="pills-hiburan" role="tabpanel" aria-labelledby="pills-hiburan-tab"
                tabindex="0">

                <section class="splide hiburan" aria-label="Splide Basic HTML Example">
                    <ul class="splide__pagination"></ul>
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <button class="splide__pagination__page" type="button" role="tab"
                                aria-controls="splide01-slide02" aria-label="Go to slide 2" tabindex="-1">
                            </button>
                        </ul>
                    </div>
                </section>

            </div>

            <div class="tab-pane fade" id="pills-travel" role="tabpanel" aria-labelledby="pills-travel-tab"
                tabindex="0">

                <section class="splide travel" aria-label="Splide Basic HTML Example">
                    <ul class="splide__pagination"></ul>
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <button class="splide__pagination__page" type="button" role="tab"
                                aria-controls="splide01-slide02" aria-label="Go to slide 2" tabindex="-1">
                            </button>
                        </ul>
                    </div>
                </section>

            </div>

            <div class="tab-pane fade" id="pills-sport" role="tabpanel" aria-labelledby="pills-sport-tab" tabindex="0">

                <section class="splide sport" aria-label="Splide Basic HTML Example">
                    <ul class="splide__pagination"></ul>
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <button class="splide__pagination__page" type="button" role="tab"
                                aria-controls="splide01-slide02" aria-label="Go to slide 2" tabindex="-1">
                            </button>
                        </ul>
                    </div>
                </section>

            </div>

            <div class="tab-pane fade" id="pills-gaming" role="tabpanel" aria-labelledby="pills-gaming-tab"
                tabindex="0">

                <section class="splide gaming" aria-label="Splide Basic HTML Example">
                    <ul class="splide__pagination"></ul>
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <button class="splide__pagination__page" type="button" role="tab"
                                aria-controls="splide01-slide02" aria-label="Go to slide 2" tabindex="-1">
                            </button>
                        </ul>
                    </div>
                </section>

            </div>

            <div class="tab-pane fade" id="pills-fashion" role="tabpanel" aria-labelledby="pills-fashion-tab"
                tabindex="0">

                <section class="splide fashion" aria-label="Splide Basic HTML Example">
                    <ul class="splide__pagination"></ul>
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <button class="splide__pagination__page" type="button" role="tab"
                                aria-controls="splide01-slide02" aria-label="Go to slide 2" tabindex="-1">
                            </button>
                        </ul>
                    </div>
                </section>

            </div>

            <div class="tab-pane fade" id="pills-youtuber" role="tabpanel" aria-labelledby="pills-youtuber-tab"
                tabindex="0">

                <section class="splide youtuber" aria-label="Splide Basic HTML Example">
                    <ul class="splide__pagination"></ul>
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <button class="splide__pagination__page" type="button" role="tab"
                                aria-controls="splide01-slide02" aria-label="Go to slide 2" tabindex="-1">
                            </button>
                        </ul>
                    </div>
                </section>

            </div>

            <div class="tab-pane fade" id="pills-tiktok" role="tabpanel" aria-labelledby="pills-tiktok-tab"
                tabindex="0">

                <section class="splide tiktok" aria-label="Splide Basic HTML Example">
                    <ul class="splide__pagination"></ul>
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <li class="splide__slide p-4">
                                <img src="assets/images/talent/talent.png" alt="img"
                                    class="max-w-[250px] w-full h-[450px] m-auto block rounded-2xl drop-shadow-xl object-cover">
                            </li>
                            <button class="splide__pagination__page" type="button" role="tab"
                                aria-controls="splide01-slide02" aria-label="Go to slide 2" tabindex="-1">
                            </button>
                        </ul>
                    </div>
                </section>

            </div>

        </div>
    </div>
</div>





<script>
var splide = new Splide('.splide.makanan', {
    perPage: 4,
    rewind: true,
    autoplay: true,
    breakpoints: {
        991.98: {
            perPage: 2,
            gap: "1rem",
            // destroy: true,
        },
        768: {
            perPage: 1,
            gap: "1rem",
            // destroy: true,
        },
    },
});

splide.mount();

var splide = new Splide('.splide.teknologi', {
    perPage: 4,
    rewind: true,
    autoplay: true,
    breakpoints: {
        991.98: {
            perPage: 2,
            gap: "1rem",
            // destroy: true,
        },
        768: {
            perPage: 1,
            gap: "1rem",
            // destroy: true,
        },
    },
});

splide.mount();

var splide = new Splide('.splide.hiburan', {
    perPage: 4,
    rewind: true,
    autoplay: true,
    breakpoints: {
        991.98: {
            perPage: 2,
            gap: "1rem",
            // destroy: true,
        },
        768: {
            perPage: 1,
            gap: "1rem",
            // destroy: true,
        },
    },
});

splide.mount();

var splide = new Splide('.splide.travel', {
    perPage: 4,
    rewind: true,
    autoplay: true,
    breakpoints: {
        991.98: {
            perPage: 2,
            gap: "1rem",
            // destroy: true,
        },
        768: {
            perPage: 1,
            gap: "1rem",
            // destroy: true,
        },
    },
});

splide.mount();

var splide = new Splide('.splide.sport', {
    perPage: 4,
    rewind: true,
    autoplay: true,
    breakpoints: {
        991.98: {
            perPage: 2,
            gap: "1rem",
            // destroy: true,
        },
        768: {
            perPage: 1,
            gap: "1rem",
            // destroy: true,
        },
    },
});

splide.mount();

var splide = new Splide('.splide.gaming', {
    perPage: 4,
    rewind: true,
    autoplay: true,
    breakpoints: {
        991.98: {
            perPage: 2,
            gap: "1rem",
            // destroy: true,
        },
        768: {
            perPage: 1,
            gap: "1rem",
            // destroy: true,
        },
    },
});

splide.mount();

var splide = new Splide('.splide.fashion', {
    perPage: 4,
    rewind: true,
    autoplay: true,
    breakpoints: {
        991.98: {
            perPage: 2,
            gap: "1rem",
            // destroy: true,
        },
        768: {
            perPage: 1,
            gap: "1rem",
            // destroy: true,
        },
    },
});

splide.mount();

var splide = new Splide('.splide.youtuber', {
    perPage: 4,
    rewind: true,
    autoplay: true,
    breakpoints: {
        991.98: {
            perPage: 2,
            gap: "1rem",
            // destroy: true,
        },
        768: {
            perPage: 1,
            gap: "1rem",
            // destroy: true,
        },
    },
});

splide.mount();

var splide = new Splide('.splide.tiktok', {
    perPage: 4,
    rewind: true,
    autoplay: true,
    breakpoints: {
        991.98: {
            perPage: 2,
            gap: "1rem",
            // destroy: true,
        },
        768: {
            perPage: 1,
            gap: "1rem",
            // destroy: true,
        },
    },
});

splide.mount();
</script>