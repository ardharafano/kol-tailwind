/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './public/assets/**/*.{html,js}',
        './public/component/**/*',
        './public/pages/**/*',
        './public/index.php'
    ],
    theme: {
        extend: {},
        // screens: {
        //     'sm': '640px',
        //     // => @media (min-width: 640px) { ... }

        //     'md': '768px',
        //     // => @media (min-width: 768px) { ... }

        //     'lg': '1024px',
        //     // => @media (min-width: 1024px) { ... }

        //     'xl': '1280px',
        //     // => @media (min-width: 1280px) { ... }

        //     '2xl': '1536px',
        //     // => @media (min-width: 1536px) { ... }

        //     'max-2xl': { 'max': '1535px' },
        //     // => @media (max-width: 1535px) { ... }

        //     'max-xl': { 'max': '1279px' },
        //     // => @media (max-width: 1279px) { ... }

        //     'max-lg': { 'max': '1023px' },
        //     // => @media (max-width: 1023px) { ... }

        //     'max-md': { 'max': '767px' },
        //     // => @media (max-width: 767px) { ... }

        //     'max-sm': { 'max': '639px' },
        //     // => @media (max-width: 639px) { ... }

        //     'minmax-sm': { 'min': '640px', 'max': '767px' },
        //     // => @media (min-width: 640px and max-width: 767px) { ... }

        //     'minmax-md': { 'min': '768px', 'max': '1023px' },
        //     // => @media (min-width: 768px and max-width: 1023px) { ... }

        //     'minmax-lg': { 'min': '1024px', 'max': '1279px' },
        //     // => @media (min-width: 1024px and max-width: 1279px) { ... }

        //     'minmax-xl': { 'min': '1280px', 'max': '1535px' },
        //     // => @media (min-width: 1280px and max-width: 1535px) { ... }

        //     'minmax-2xl': { 'min': '1536px' },
        //     // => @media (min-width: 1536px) { ... }
        // },
        fontFamily: {
            'Montserrat': ['Montserrat', 'sans-serif'],
        }
    },
    plugins: [],
}